

<?php
// include Database connection file
include("db_connection.php");

// Design initial table header
$data = '
<aside id="tabs" class="col-lg-3">
    <h1 class="titleSmall">Nyheter</h1>
    <ul id="caselist">';


$query = "SELECT * FROM users";

if (!$result = mysqli_query($con, $query)) {
    exit(mysqli_error($con));
}

// if query results contains rows then featch those rows
if(mysqli_num_rows($result) > 0)
{
    $number = 1;
    while($row = mysqli_fetch_assoc($result))
    {

        $data .= '<li><a href="#tabs-'.$number.'">'.$row['first_name'].'</a></li>';
        $number++;
    }
}
else
{
    // records now found
    $data .= '<tr><td colspan="6">Records not found!</td></tr>';
}

$data .= '    </ul>
            </aside>';

echo $data;
?>



