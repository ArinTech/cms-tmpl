<?php
// include Database connection file
include("db_connection.php");

// Design initial table header
$data = '<table class="table table-bordered table-striped">';


$query = "SELECT * FROM users";

if (!$result = mysqli_query($con, $query)) {
    exit(mysqli_error($con));
}

// if query results contains rows then featch those rows
if(mysqli_num_rows($result) > 0)
{
    $number = 1;
    while($row = mysqli_fetch_assoc($result))
    {
        
        $data .= '<div id="tabs-'.$number.'" aria-labelledby="ui-id-1" role="tabpanel" class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="false" style="display: none;">'
            .'<h1 class="titleSmall">'.$row['first_name'].'</h1>'
            .'<p>'.$row['last_name'].'</p>'
            .'</div>';
            $number++;
    }
}
else
{
    // records now found
    $data .= '<tr><td colspan="6">Records not found!</td></tr>';
}

$data .= '</table>';

echo $data;
?>