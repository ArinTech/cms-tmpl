$( document ).ready(function() {
    
manageData();

/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: '../production/api/getData.php'
    }).done(function(data){

    	manageRow(data.data);

    });

}

/* Get Page Data*/
function getPageData() {
	$.ajax({
    	dataType: 'json',
    	url: '../production/api/getData.php',
	}).done(function(data){
		manageRow(data.data);
	});
}

/* Add new Item table row */
function manageRow(data) {
	var	rows = '';
	$.each( data, function( key, value ) {
	  	rows = rows + '<tr>';
	  	rows = rows + '<td>'+value.rubrik+'</td>';
	  	rows = rows + '<td>'+value.artikel+'</td>';
        rows = rows + '<td>'+value.datum+'</td>';
	  	rows = rows + '<td data-id="'+value.id+'">';
        rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Ändra</button> ';
        rows = rows + '<button class="btn btn-danger remove-item">Ta bort</button>';
        rows = rows + '</td>';
	  	rows = rows + '</tr>';
	});

	$("tbody").html(rows);
}

/* Create new Item */
$(".crud-submit").click(function(e){
    e.preventDefault();
    var form_action = $("#create-item").find("form").attr("action");
    var rubrik = $("#create-item").find("textarea[name='rubrik']").val();
    var artikel = $("#create-item").find("textarea[name='artikel']").val();
    var datum = $("#create-item").find("input[name='datum']").val();
    var id = $("#create-item").find("select[id='id']").val();


    

    if(id != '' && rubrik != '' && artikel != ''){
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: '../production/api/create.php',
            data:{rubrik:rubrik,artikel:artikel,datum:datum}
        }).done(function(data){
            $("#create-item").find("textarea[name='rubrik']").val('');
            $("#create-item").find("textarea[name='artikel']").val('');
            $("#create-item").find("input[name='datum']").val('');
            $("#create-item").find("select[id='id']").val("");
            $("#create-item").hide();
            $(".modal-backdrop").hide(); 
            manageData();
            toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 3000});

        });
    }else{
        alert('You are missing title or description.')
    }

});

/* Remove Item */
$("body").on("click",".remove-item",function(){
    var id = $(this).parent("td").data('id');
    var c_obj = $(this).parents("tr");

    $.ajax({
        dataType: 'json',
        type:'POST',
        url:  '../production/api/delete.php',
        data:{id:id}
    }).done(function(data){
        c_obj.remove();
        toastr.success('Artikel har tagits bort.', 'Success Alert', {timeOut: 5000});
        getPageData();
    });

});

/* Edit Item */
$("body").on("click",".edit-item",function(){

    var id = $(this).parent("td").data('id');

     $.ajax({
        dataType: 'json',
        type:'GET',
        url:  '../production/api/getdatabyid.php',
        data:{id:id}
    }).done(function(x){
        $("#edit-item").find("textarea[name='rubrik']").val(x.data[0].rubrik);
        console.log(x.data[0].rubrik);
        $("#edit-item").find("textarea[name='artikel']").val(x.data[0].artikel);
        $("#edit-item").find("input[name='datum']").val(x.data[0].datum);
        $("#edit-item").find(".edit-id").val(id);


    });



});

/* Updated new Item */
$(".crud-submit-edit").click(function(e){

    e.preventDefault();
    var form_action = $("#edit-item").find("form").attr("action");
    var rubrik = $("#edit-item").find("textarea[name='rubrik']").val();
    var datum = $("#edit-item").find("input[name='datum']").val();
    var artikel = $("#edit-item").find("textarea[name='artikel']").val();
    var id = $("#edit-item").find(".edit-id").val();

    if(rubrik != '' && artikel != ''){
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: '../production/api/update.php',
            data:{rubrik:rubrik,artikel:artikel,datum:datum,id:id}
        }).done(function(data){
            getPageData();
            $(".modal").modal('hide');
            $("#edit-item").hide();
            $(".modal-backdrop").hide(); 
            toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 5000});
        });
    }else{
        alert('You are missing title or description.')
    }

});
});