<aside id="cases" class="col-lg-3">
   <h1 class="titleSmall">Q&A</h1>
   <ul id="caselist">
      <li><a href="#tabs-10">Hur fungerar TMPL?</a></li>
      <li><a href="#tabs-1">Vad kostar det att använda TMPL?</a></li>
      <li><a href="#tabs-2">Kan jag få in TMPL i mitt lägenhet?</a></li>
      <li><a href="#tabs-3">Måste man ha en egen dator för att använda TMPL</a></li>
      <li><a href="#tabs-4">Behövs det en egen server för att TMPL ska fungera?</a></li>
      <li><a href="#tabs-5">Vad krävs för att installera TMPL?</a></li>
      <li><a href="#tabs-6">Vem kan se vad jag skriver i TMPL</a></li>
      <li><a href="#tabs-7">Kan mina grannar se min el- och vattenförbrukning?</a></li>
      <li><a href="#tabs-8">Kan jag radera det jag har skrivit i TMPL?</a></li>
      <li><a href="#tabs-9">Jag har idéer om nya funktioner, vad gör jag med dem?</a></li>
      <li><a href="#tabs-11">Hur kan lokala företag som exempelvis affärer och restauranger få vara med i grannskapets TMPL-meny</a></li>
     <!--   <li><a href="#tabs-6">Eiusmod Tempor</a></li> -->
      <!--  <li><a href="#tabs-7">Incididunt labore</a></li> -->
   </ul>
</aside>