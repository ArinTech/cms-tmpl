<a name="bottom"></a>
<footer id="bottomBar" class="container-fluid">
   <div class="row panel-body form-horizontal" ng-controller="ContactController">
      <div class="col-xs-12 col-lg-8 aside1">
         <form role="form" id="contactForm" data-toggle="validator" class="shake">
         <h5 class="adressTitle" id="contactTitle">Ska vi höras?</h5>
            <div class="row stylecontacts">
               <div class="col-sm-7 footer">
               </div>
               <div class="form-group col-sm-7 formpad">
                  <label for="inputName" class="h4"></label>
                  <input type="text" class="form-control col-lg-10 inputOne" id="name" placeholder="Namn" required data-error="NEW ERROR MESSAGE">
                  <div class="help-block with-errors"></div>
               </div>
               <div class="form-group col-sm-7 formpad">
                  <label for="inputEmail" class="h4"></label>
                  <input type="inputEmail" class="form-control col-lg-10 inputOne" id="email" placeholder="Mailadress" required>
                  <div class="help-block with-errors"></div>
               </div>
               <div class="form-group col-sm-7 formpad">
                  <label for="inputMessage" class="h4 "></label>
                  <textarea id="message" class="form-control col-lg-10 inputOne" placeholder="Meddelande" required></textarea>
                  <div class="help-block with-errors"></div>
               </div>
               <div class="form-group col-sm-7 buttonform">
                  <input type="submit" id="form-submit"" class="btn btn-success btn-lg submitbutton" id="buttonSubmit" value="Skicka"/>
               </div>
               <div id="msgSubmit" class="form-group col-sm-7 "></div>
               <div class="clearfix"></div>
            </div>
         </form>
      </div>
      <aside class="col-xs-12 col-lg-2" id="contactInfo">
         <div class="contactbox">
            <h5 class="adressTitle" id="adresshead">Adress</h5>
            <p class="adress">TMPL Solutions AB<br>Villavägen 7<br>752 36 Uppsala<br>info (at) tmpl.se</p>
         </div>
         <div id="appdownloadDesk">
            <a href="" class=""><img src="img/android-app-on-google-play-seeklogo.png" class="andrbutton" alt=""></a>
            <a href="" class=""><img src="img/available-on-the-iphone-app-store-seeklogo.png" class="andrbutton" alt=""></a>
         </div>
         <div class="linkedinbutton">
              <a href="https://www.linkedin.com/company/15085527?trk=tyah&amp;trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A15085527%2Cidx%3A2-4-5%2CtarId%3A1477928994437%2Ctas%3Atmpl" target="_blank"><img src="img/linkedinlogo.png" alt="fb" class="icon facebook"></a>
                <br>
                 <br>
                 <p class="copyright adress contactbox col-x-12">© 2016 TMPL</p>
         </div>
              <!-- ´Tiwtter  <a href="https://twitter.com/"></a><img src="img/twitter.png" alt="" class="icon twitter">-->
      </aside>
   </div>
</footer>
