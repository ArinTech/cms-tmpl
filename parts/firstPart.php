<section id="geninfo" class="container-fluid">
   <div class="row justify">
      <div class="col-xs-12 col-lg-6 col-lg-height boxOne card" id="social">
         <ul class="bxslider">
            <li class="something"> 
            <h2 class="marginshead">Socialt</h2>
            <img src="img/chatbox.png" alt="chatbox" class="imgBig">
            <div class="textWithArrow socialfront">
               <p><span id="socialtext" class="teamtext">Är det någon som vill ta en hundpromenad eller jogga ikväll? Tryggheten och sammanhållningen i ett bostadsområde kan öka med en gemensam plattform för kommunikation och kontakt grannar emellan.</span>
               </p>
            </div>
            </li> 
            <li class="something">
               <h2></h2>
               <p id="fliptext1">Anslagstavlans och kvartersfesternas tid är förbi. Idag behövs nya redskap för att dela information och för att skapa en känsla av sammanhållning, tillit och trygghet i ett bostadsområde. Snabbt växande stadsdelar samlar många människor som inte känner varandra sedan tidigare. Vi tillbringar också allt mer tid på nätet. Sociala medier som exempelvis Facebook har blivit allt viktigare som sammanhållande länk i en värld där många är i rörelse. Därför är en plattform konstruerad enligt samma princip, ett snabbt, modernt och inkluderande sätt att skapa nya möjligheter för kommunikation och delaktighet mellan grannar, hyresvärd och bostadsrättsförening.
               Plattformen TMPL erbjuder verktyg som gör det möjligt för användarna att dela information, planera att träffas, rapportera skador, låna saker eller byta tjänster och resurser med varandra. Användarna kan interagera, kommentera, rösta i enkäter eller svara på förfrågningar. De enkla vardagskontakterna kan medverka till att människor får ökat förtroende för varandra, och att hela bostadsområdet upplevs som mer tryggt och säkert. <br>
               </p>
            </li>
         </ul>
      </div> 
      <div class="col-xs-12 col-lg-6" id="smallerOne">
         <div class="col-xs-12 col-lg-12 boxOne card" id="miijo">
            <ul class="bxslider">
               <li class="something2"> 
                  <div class="col-lg-6 col-xs-12 miljobox">
                     <h2 class="marginshead">Miljövänligt</h2>
                     <img src="img/leaf.png" alt="chatbox" class="imgSmall mobileImg">
                     <div class="textWithArrow">
                        <p><span class="teamtext"> Skräddarsydda tips kring el- och vattenförbrukning gör det möjligt att välja en mer klimatsmart livsstil. Genom att låna och byta saker med varandra minskar vi svinn och slöseri.
                        </span></p>
                     </div>
                  </div>
                  <img src="img/leaf.png" alt="chatbox" class="imgSmall deskImg">
               </li> 
               <li class="something2">
                  <h2></h2>
                  <p id="fliptext2">Att bygga och bo slukar en femtedel av världens energiförbrukning. Varje insats för att minska förbrukningen är viktig, även det enskilda hushållets. TMPL kan mäta varje lägenhets förbrukning av el och vatten, och ge skräddarsydda tips om sätt att sänka konsumtionen. På så sätt sparas både energi och pengar. Genom smart teknik, gemensamma resurser och information, vill vi uppmuntra hållbara vanor och livsval, och ett mer långsiktigt hållbart samhälle. <br>
                  </p>
               </li>
            </ul>
         </div> 
         <div class="col-xs-12 col-lg-12 card" id="ekonomi">
            <ul class="bxslider">
               <li class="something2"> 
                  <div class="col-lg-6 col-xs-12">
                     <h2 class="marginshead" id="ekohead">Ekonomiskt</h2>
                     <img src="img/graph.png" alt="leaf" class="imgSmall mobileImg" id="lovet">
                     <div class="textWithArrow">
                        <p><span class="teamtext"> Att köpa, sälja eller låna prylar av varandra spar pengar och naturresurser. Vänskapliga tävlingar om vem som lever mest energismart är bra både för miljön och för plånboken.</span>
                        </p>
                     </div>
                  </div>
                  <img src="img/graph.png" alt="graph" class="imgSmall deskImg" id="grafen"> 
               </li> 
               <li class="something2">
                  <h2></h2>
                  <p id="fliptext3">Förutom att hjälpa de boende spara pengar genom mindre förbrukning av el och vatten uppmuntrar plattformen TMPL också den informella ekonomin i boendeområdet. Den blir en smidig kanal för att sinsemellan köpa och sälja varandras överflödiga prylar, eller byta saker eller enkla tjänster med varandra. Det går också lätt att planera samåkning och gemensamma storköp eller efterlysa något som bara behövs då och då. Varje hushåll behöver kanske inte en egen borrmaskin?
                  </p>
               </li>
            </ul>                        
         </div>  
      </div>
   </div>      
</section>