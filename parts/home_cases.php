<section id="extraInfo" class="container-fluid">
   <div class="row">
      <p id="cases2">Cases</p>
      <div id="firstExtra" class="col-lg-6 col-lg-offset-3">
      <a class="page-scroll" href="cases.php" ">
         <h2 class="marginshead3" style="padding-top:0px; margin-top:20px">Modernt och naturnära - att bo i Smaragden.</h2>
         <p id="" style="text-decoration:none; color:#607D8B;">Stadsdelen Rosendal i södra Uppsala är både citynära och prunkande grönt, välbeläget strax intill Stadsskogen. Nybyggda bostadsrättsföreningen Smaragden i Rosendal består av 114 moderna ettor och tvåor, som är särskilt utvecklade för aktiva människor som gärna samverkar med sina grannar. Alla lägenheterna i Smaragden är utrustade med TMPL, som gemensam plattform för kommunikation och interaktion grannarna emellan.</p>
      </a>    
      </div>
      <div>
         <img class="smaragd" src="img/Smaragden.jpg">
      </div>
      <div id="secondExtra" class="col-xs-12 col-lg-12">
         <div class="col-lg-10 col-lg-offset-1 phonebox">
            <div class="col-xs-12 phonepart">
               <div class="col-lg-9">
                  <img src="http://tmpl.se/img/tmpl_mockup.png" alt="phonesDesk" class="superBgPicDesk">
               </div>   
               <div class="phonetext col-lg-3">
                  <h2 class="col-xs-12" id="vadvigor">Är du också<br> intresserad av TMPL?</h2>
                  <img src="http://tmpl.se/img/tmpl_mockup.png" alt="phonesMob" class="superBgPic col-xs-10">
                  <p class="col-xs-12 col-lg-12" id="celltext">Hittills har vi fått överväldigande respons för plattformen TMPL från så väl boende som andra fastighetsutvecklare. Vi erbjuder också andra fastighetsbolag och -utvecklare tillgång till teknikplattformen genom en så kallad white label-licens. Det innebär att man kan anpassa plattformens och mobilappens utseende i enlighet med den egna grafiska profilen. Vi har strävat efter att göra det så enkelt och tidseffektivt som möjligt för licensinnehavare att sjösätta systemet. Om du vill veta mer om vår licensmodell, kontakta oss via formuläret längst ner på sidan!!</p>
               </div>
            </div>
         </div>
      </div>
   </div>   
</section>