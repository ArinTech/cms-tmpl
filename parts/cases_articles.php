<article id="titleSmall" class="col-lg-6">
   <div id="tabs-10">
        <h1 class="titleSmall">Smaragden</h1>
        <h1 id="casesline">Modernt och naturnära - att bo i Smaragden</h1>
         <p>Stadsdelen Rosendal i södra Uppsala är både citynära och prunkande grönt, välbeläget strax intill Stadsskogen. Nybyggda bostadsrättsföreningen Smaragden i Rosendal består av 114 moderna ettor och tvåor, som är särskilt utvecklade för aktiva människor som gärna samverkar med sina grannar. Alla lägenheterna i Smaragden är utrustade med TMPL, som gemensam plattform för kommunikation och interaktion grannarna emellan. <br> <br>
         Bostadsområdet Smaragden i södra Uppsala är både citynära och prunkande grönt. Byggnaden består av 114 moderna ettor och tvåor, och ligger nära stan men också nära Stadsskogen. Det är särskilt utvecklat för aktiva människor som gärna samverkar med sina grannar. Alla lägenheterna i Smaragden är utrustade med TMPL, som gemensam plattform för kommunikation och interaktion grannarna emellan.<br>
         – Jag gillar smarta tekniklösningar, jag är väl lite av en early adopter, säger Elisabet Långström, som bor i en etta i Smaragden.
         Hon arbetar med kommunikation på Uppsala Universitet, men sitter också i bostadsrättsföreningens styrelse. Därför tycker hon att det är extra bra att ha en gemensam kommunikationskanal med de andra bostadrättsinnehavarna, och hon är själv inne i TMPL i stort sett varje dag.<br>
         - Jag kollar in om det har hänt något, kommunicerar, och löser problem.<br><br>
         <img src="img/Smaragden.jpg" alt="phonesDesk" class="superBgPicDesk2">
         <img src="img/Smaragden.jpg" alt="phonesMob" class="superBgPic col-xs-11"> <br><br><br>
         Än så länge är det ofta praktiska frågor till styrelsen som dyker upp, till exempel frågor om soprum och förråd. Men i framtiden ser Elisabet Långström fram emot att använda TMPL på många flera sätt, framför allt för att stärka gemenskapen i området.<br>  <br>
         - Det finns många hundägare här, och jag kan tänka mig att efterlysa promenadsällskap eller hundvakt. Eller kanske arrangera en springgrupp i stadsskogen?<br>

         TMPL kan samtidigt ge de boende möjlighet att bli mer miljömedvetna. Appen mäter och ger råd om förbrukning av el- och vatten, och kan också bli en kanal för grannars byteshandel och återbruk av prylar. Det går också att boka bilar i den gemensamma bilpoolen, beställa mat via appen från någon av de lokala restaurangerna, eller handla i den matbutik som inom kort ska flytta till området.<br><br>
 
         Lydia Karlefors är ansvarig för tidiga skeden och hållbarhet på Rosendal Fastigheter som utvecklat och byggt Smaragden. Miljö och smarta tekniska lösningar går som en röd tråd genom bostadsbolagets utvecklingsarbete, berättar hon, och betonar hur viktigt det var att hållbarhet var en del av projektet Smaragden från första start.<br>
         - Vår vision är att skapa riktigt bra lösningar när det gäller innovativ design, hälsosamma miljöer och gröna lösningar för alla våra boendeprojekt. Finns det inte hållbarhetsfrågorna med tidigt finns en risk att de glöms eller kompromissas bort. Miljöfrågor får inte bara bli dekorationer på slutet.<br><br>

         Smart teknik kan både vara en sporre att tänka mer miljövänligt, och samtidigt rolig, enligt Lydia Karlefors. Hon syftar på så kallad gamification, alltså digital teknik med inbyggda tävlingsmoment. Och om Pokémon Go har fått människor att promenera mera för att fånga flest pokémons, kanske en app som TMPL kan få grannar att tävla i vem som sparar el och vatten mest och bäst.<br> 
         - TMPL mäter ju varje lägenhets enskilda förbrukning av el och vatten, och ger spartips. Det kan man också tävla i. Visst vore det roligt att kunna vinna över dina grannar, som kvarterets mest miljömedvetna?
   </div>
   <div id="tabs-1">
      <h1 class="titleSmall">Service i Rosendal</h1>
      <h1 id="casesline">All service i Rosendal direkt i din telefon</h1>
      <p>Appen TMPL är en miljövänlig och smidig kontaktkanal grannar emellan, men också en smidig länk mellan de boende och alla serviceinrättningar i bostadsområdet. Restauranger, gym och butiker kopplas upp mot TMPL, och de boende kan boka, beställa och handla direkt från sin egen mobil. <br><br>
      Den som till exempel vill köpa hämtmat öppnar TMPL-appen i telefonen, och beställer online med några enkla knapptryck.<br><br>
      - Det är mycket smidigare för våra kunder än att ringa, säger Zhiva Bergström, som driver Rosendals sushi.  <br>
      - Det säger pling i vår app i restaurangen när beställningen kommer. Vi skickar snabbt ett kvitto till kunden med besked om vilken tid maten är klar för avhämtning, så att de inte behöver stå och vänta.<br><br>
      Serviceutbudet i Rosendal växer snabbt i takt med att allt fler boende flyttar in. I april 2017 kommer ICA Nära att öppna en ny butik som ska vara miljösmart i alla led. Det kommer till exempel att gå att beställa direkt ur butikens sortiment via appen TMPL. Johan Schartau är en erfaren livsmedelshandlare som tidigare bland annat drivit ICA-butiker i Sävja och Knivsta. Nu blir han föreståndare för den nya butiken i Rosendal och ser fram emot att lära känna sina blivande kunder.  <br><br>
      - Kundkontakten är bland det roligaste jag vet, och jag vill gärna ha hjälp av mina blivande kunder att hitta rätt i sortimentet, vad de helst vill kunna köpa, säger Johan Schartau, och betonar att ekologiska och närodlade produkter ska få ta stor plats.<br><br>
      Men inte bara maten ska vara grön och hållbar. Hela butiken ska orsaka så lite klimatpåverkan som möjligt. <br><br>
      - Hela Rosendalsområdet andas hållbarhet, och vårt ansvar för hållbarhet visar sig också i form av den teknik vi använder. ICA Nära Rosendal ska självklart ha miljövänliga kylar, frysar och belysning, en klok sophantering och minimalt matsvinn, säger Johan Schartau.<br><br>
      SEB USIF Arena är hem för Uppsala Studenters Idrottsförening, och ligger i hjärtat av Rosendal. Fem tennisbanor, squashbana och gym är något av det som ryms i den 8 000 kvadratmeter stora arenan, som också innehåller restaurang, ett salt-spa och kontor. Mycket av aktiviteterna riktas mot barn och unga, men träningssugna i alla åldrar är välkomna. Det vackert böljande taket på arenan är täckt av ett lager av växten sedum, så taket klimatkompenserar koldioxid lika effektivt som 750 träd.<br><br>
      Idag går det att boka tennis- och padeltider via TMPL, berättar Rikard Wendt. Han är ställföreträdande verksamhetschef USIF Arena, och planerar nu hur fler aktiviteter på USIF Arena ska gå att boka via appen.<br><br>
      - Det blev snabbt väldigt populärt när vi lanserade möjligheten att boka tider för padel och tennis hemifrån, och jag ser gärna att vi utökar. I nästa steg kan vi kanske göra det möjlighet att boka en plats på någon av våra gruppträningar eller tid med en personlig tränare via TMPL, säger Rikard Wendt. </p>
   </div>   
   <div id="tabs-2">
      <h1 class="titleSmall"></h1>
      <h1 id="casesline"></h1>
      <p> </p>
  </div>
   <div id="tabs-3">
      <h1 class="titleSmall"></h1>
      <h1 id="casesline"></h1>     
      <p> </p>
  </div>
  <div id="tabs-4">
      <h1 class="titleSmall"></h1>
      <h1 id="casesline"></h1>         
      <p> </p>
   </div>
   <div id="tabs-5">
      <h1 class="titleSmall"></h1>
      <h1 id="casesline"></h1> 
      <p> </p>
   </div>      
   <div id="tabs-6">
      <h1 class="titleSmall"></h1>
      <h1 id="casesline"></h1>    
      <p></p>
   </div>    
   <div id="tabs-7">
      <h1 class="titleSmall"></h1>
      <h1 id="casesline"></h1>
      <p></p>
   </div>     
   <div id="tabs-8">
      <h1 class="titleSmall"></h1>
      <h1 id="casesline"></h1> 
      <p></p>
   </div>      
   <div id="tabs-9">
      <h1 class="titleSmall"></h1>
      <h1 id="casesline"> </h1>  
      <p></p>
   </div>
</article>   