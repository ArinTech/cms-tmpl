<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="edscription" content="">
    <meta name="author" content="">

    <title>TMPL</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Plugin CSS -->

    <link rel="stylesheet" href="vendor/device-mockups/device-mockups.min.css">

    <!-- Bxslider CSS --> 
    <link href="css/jquery.bxslider.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Slab:200,300,400" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css?<?php echo time();?>">
       
</head>