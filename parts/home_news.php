<section id="additInfo" class="container-fluid">
   <div class="row addOne">
      <p id="nyheter">Nyheter</p>
      <ul class="bxSliderAdd" style="list-style-type:none">
         <li class="col-xs-12 col-lg-4" id="student">
            <a class="page-scroll" href="news.php">  
               <h2 class="marginshead2">DN och UNT</h2> 
               <p class="col-lg-11 nyheterna" id="newstext">Kul! Nu har både DN och UNT uppmärksammat TMPL och hur vår lösning fungerar i praktiken.
               </p>
               <p style="position:absolute; bottom:0px; left:30px;font-family:'Roboto Slab';" id="newstext">läs mer</p>
            </a>   
         </li>
         <li class="col-xs-12 col-lg-4" id="comhem" >
            <a class="page-scroll" href="/news.php">  
               <h2 class="marginshead2">Ministerbesök</h2>             
               <p class="col-lg-11 nyheterna" id="newstext">Fastigheten Smaragden fick idag fint besök av miljöminister Karolina Skog och Peter Eriksson, bostads- och digitaliseringsminister. De båda ministrarna imponerades av hur man kan leva och bo mer hållbart och smart tack vare appen TMPL.
               </p>
               <p style="position:absolute; bottom:0px; left:30px; font-family:'Roboto Slab';" id="newstext">läs mer</p>
            </a> 
         </li>
         <li class="col-xs-12 col-lg-4" id="rosendal">
            <a class="page-scroll" href="news.php">     
               <h2 class="marginshead2">BRF Grindstugan</h2>
               <p class="col-lg-11 nyheterna" id="newstext">Uppsalas mest prestigefulla bostadsprojekt kommer att utrustas med smart teknik för ett modernt, trivsamt och resurssmart sätt att bo. De boende får tillgång till en rad effektiva och miljövänliga tjänster med hjälp av appen TMPL som de använder genom sin telefon.
               </p>
               <p style="position:absolute; bottom:0px; left:30px; font-family:'Roboto Slab';" id="newstext">läs mer</p> 
            </a>
         </li>
      </ul>
   </div>
</section>