<section class="container-fluid" id="whybox">
   <div class="row">
      <div class="col-xs-12 col-lg-4" id="why">
         <p id="teamet">Om teamet bakom TMPL</p>
         <img class="col-lg-3" id="bigteam" src="img/team_illu (1).svg">
         <div id="teamtext2">Vägen till en hållbar framtid börjar just här. Vi som utvecklar TMPL har tidigare bland annat arbetat i ledande företag som Skanska, Apple, Nokia och Cycore. Nu har vi samlats kring en vision om hur bostäder och bostadsområden kan utvecklas, och vilka krav och behov boende kommer att ha i framtiden. En av våra viktigaste drivkrafter är insikten om vårt gemensamma ansvar för att idag, här och nu, bygga goda livsmiljöer och en hållbar framtid.
         </div>
      </div>   
      <img class="col-lg-3" id="tmplteam" src="img/team_illu (1).svg">
   </div>
</section>