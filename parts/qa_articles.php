<article id="titleSmall" class="col-lg-6">
   <div id="tabs-10">
      <h1 class="titleSmall">Hur fungerar TMPL?</h1>
      <h1 id="casesline">Social medie-plattform 2.0</h1>
      <p>TMPL är en social medie-plattform som låter dem som bor i ett hus eller bostadsområde kommunicera med sina grannar i ett flöde som liknar det på Facebook. Man kan boka gemensamma resurser som tvättstuga, uteplats eller övernattningsrum, eller köpa, sälja eller ge bort saker via TMPLs marknadsplats.<br> När det är dags för grillfest eller årsmötet i bostadsrättsföreningen kan inbjudan skickas ut via TMPL, som automatiskt registrerar tiden i telefonens inbyggda kalender. Den som är hungrig kan enkelt beställa från lokala restaurangers menyer eller köpa en matkasse från närmaste livsmedelsbutik. Inbyggda mätare håller koll på varje lägenhets el- och vattenförbrukning, och ger anpassade spartips. Den som vill kan tävla mot andra som bor i liknande lägenheter om vem som är mest energismart. Snart kommer funktioner som kontrollerar låset i ytterdörren, visar när man fått post i brevlådan eller som gör det möjligt att sätta på kaffebryggaren på distans, så att kaffet är klart när man kommer hem. (Vissa av tjänsterna ovan kräver att infrastrukturen i huset du bor stödjer detta)
      <br><br>
      </p>
   </div>
    
   <div id="tabs-1">
      <h1 class="titleSmall">Vad kostar det att använda TMPL?</h1>
      <h1 id="casesline">Gratis för privatpersoner</h1>
      <p>För dig som bor i en TMPL-fastighet är appen gratis, det är din hyresvärd eller BRF som beslutar om hur kostnaden skall tas.
      <br><br>

      -En bostadsrättsförening betalar en anslutningsavgift och därefter en månadsavgift för varje ansluten lägenhet.  
       <br><br>
      -Hyresvärdar/fastighetsutvecklare som vill ha en egen version/licens av plattformen betalar en övergripande anpassningsavgift och sen en månadsavgift för varje ansluten lägenhet. I anpassningsavgiften ingår modifiering till önskad design och önskade funktioner/moduler. 
      <br><br>
      </p>
   </div>
   <div id="tabs-2">
      <h1 class="titleSmall">Kan jag få in TMPL i mitt lägenhet?</h1>
      <h1 id="casesline">TMPL är för alla</h1>
      <p>Ja, det kan du. Prata med din hyresvärd eller bostadsrättsförening och be dem kontakta oss. Din förening, fastighet- eller hyresvärd skall först ansluta sig till TMPL och därefter har du tillgång till plattformen.
      <br><br>
      </p>
   </div>

   <div id="tabs-3">
      <h1 class="titleSmall">Måste man ha en egen dator för att använda TMPL?</h1>
      <h1 id="casesline">Vi följer den mobila utvecklingen</h1>     
      <p>Nej, det räcker med en Android eller iPhone-mobil.
      <br><br>
      </p>
   </div>
   <div id="tabs-4">
      <h1 class="titleSmall">Behövs det en egen server för att TMPL ska fungera?</h1>
      <h1 id="casesline">Vi har gjort det enkelt för er</h1>         
      <p>Nej, allt ligger ligger lagrat i vår molntjänst.
      <br><br>
      </p>
   </div>
   <div id="tabs-5">
      <h1 class="titleSmall">Vad krävs för att installera TMPL?</h1>
      <h1 id="casesline">En smartphone är det som krävs</h1> 
      <p>Tekniska förutsättningar och krav för att installera TMPL idag, är att du har en iPhone 4s eller senare modell (minimum krav iOS 9) eller Android (minimum version 4.4.3).
      <br><br> 
      </p>
   </div>      
    
   <div id="tabs-6">
      <h1 class="titleSmall">Vem kan se vad jag skriver i TMPL</h1>
      <h1 id="casesline">Du är skyddad i TMPL miljön</h1>    
      <p>Det du skriver i TMPL syns enbart för dig och dina grannar som är uppkopplade inom samma förening/bostad.
      <br><br> 
      </p>
   </div>    
    
   <div id="tabs-7">
      <h1 class="titleSmall">Kan mina grannar se min el- och vattenförbrukning? </h1>
      <h1 id="casesline">Du har kontorll över din lägenhet och dess inehåll</h1>
      <p>Nej, din förbrukning är enbart kopplad till din lägenhet. Bara du och de andra som bor med dig i lägenheten kan se förbrukningen 
      <br><br> 
      </p>
   </div>     
   <div id="tabs-8">
      <h1 class="titleSmall">Kan jag radera det jag har skrivit i TMPL? </h1>
      <h1 id="casesline">Vett och etikett</h1> 
      <p>Ja det kan du, du kan även rapportera olämpliga meddelanden skrivna av andra grannar.
      Om du skulle flytta från lägenheten raderas även alla användardata automatiskt.
      <br><br> 
      </p>
   </div>      
   <div id="tabs-9">
      <h1 class="titleSmall">Jag har idéer om nya funktioner, vad gör jag med dem? </h1>
      <h1 id="casesline">TMPL och Rosendal Fastigheter inleder samarbete </h1>  
      <p>Så roligt! TMPL är byggd för att öka användarvänligheten hos just sina användare, och alla förslag är välkomna. Kontakta oss på info@tmpl.se
      <br><br>
      </p>
   </div>      
   <div id="tabs-11">
      <h1 class="titleSmall">Hur kan lokala företag som exempelvis affärer och restauranger få vara med i grannskapets TMPL-meny</h1>
      <h1 id="casesline">Vi gynnar alla lokala serviceinrättningar</h1>  
      <p>Lokala serviceinrättningar kan ansluta sig till TMPL genom att kontakta oss på info@tmpl.se, Vi guidar er genom implementering av meny och produkter, och visar hur ordersystemet fungerar. 
      <br><br>
      </p>
   </div>     




    <!--  <div id="tabs-6">
      <h1 class="titleSmall">Eiusmod Tempor</h1>
      <p>666Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
    </div> 

    <div id="tabs-7">
      <h1 class="titleSmall">Incididunt labore</h1>
      <p>777Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
    </div> -->

  </article>
</section> 