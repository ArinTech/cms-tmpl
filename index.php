<?php
include 'parts/head.php';
?>
<body id="page-top" ng-app="contactApp">
	<section class="container-fluid boxOne" id="first">  
		<header>
			<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		        <div class="container-fluid">
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		                	<span class="sr-only">Toggle navigation</span> &#9776; <i class="fa fa-bars"></i>
		                </button>
		                <a class href="index.php" id="logobox"><img src="img/Logo Tmpl.png" alt="logo" class="imgSmall" id="logoPicture"></a>
		            </div>
			        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			            <ul class="nav navbar-nav navbar-right" style="margin-right:25px">
			                <li>
			                    <a class="page-scroll" href="cases.php">Cases</a>
			                </li>
			                 <li>
			                    <a class="page-scroll" href="news.php">Nyheter</a>
			                 </li>
			                 <li>
			                    <a class="page-scroll" href="qa.php">Q&A</a>
			                </li>                 
			                </ul>
			        </div>
		        </div>
		   </nav>
		</header>
		<div id="topinfo" class="container-fluid">
			<div class="row" id="toprow">
				<div class="col-lg-4 col-xs-12">
					<h1 class="col-xs-12 col-lg-8" id="big">Smarta hem. Smartare grannar.</h1>
					<img src="img/tmplillu.gif" alt="house" class="col-xs-12" id="housemob">
					<p class="col-lg-7 col-xs-12" id="toptext">TMPL är en tjänst som skapar ett bättre och mer hållbart boende. Tekniska lösningar som samsas med en social plattform där både grannar, näringsidkare och fastighetbolag samverkar med varandra. Kärnan i tjänsten är en lättbegriplig och kraftfull app där användaren har fullständig kontroll över alla delar i sitt boende.

Idag är inte mobilen bara en telefon. Och nu kan du med en enda app använda den för att släcka lampor, starta kaffebryggaren, reglera inomhustemperaturen, köpa en cykel, ta fram manualen till diskmaskinen, låsa upp dörren, boka tid hos husläkaren, handla matkassen, bjuda in till gårdsfest och få den senaste informationen från din bostadsrättsförening, hyresvärd eller samfällighet.</p>
					<div id="appdownloadDesk">				
	               <a href="#bottom" class="" role="button""> <img src="img/ladda_ner_knapp.svg" class="andrbutton2"></a> 
	            </div>
					<div id="appdownload">
						<a href="" class=""><img src="img/android-app-on-google-play-seeklogo.png" class="andrbutton" alt=""> </a>
						<a href=""><img src="img/available-on-the-iphone-app-store-seeklogo.png" class="andrbutton" alt=""></a>
					</div>
				</div>	
				<div class="col-lg-8" id="imgForDesk">
	         	<img src="img/tmplillu.gif" alt="housedesk" id="housedesk">
	         </div>
			</div>
		</div>
	</section>
<?php 
include 'parts/firstPart.php';
include 'parts/home_cases.php';
include 'parts/home_news.php';
include 'parts/why.php';
include 'parts/footer.php';
include 'parts/script.php'; 		





