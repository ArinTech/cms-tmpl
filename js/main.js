(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs


    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 50
        }
    })

}); // End of use strict


/*

function clickToFlip() {
	$(this).parent().flip();
}


$(function() {
	$("#arrow1").on("click", clickToFlip);
});

*/

function flip() {
  $(this).parent().toggleClass('flipped');
   $(this).prev().toggleClass('hidethis');
  $(this).parent().children().first().toggleClass('hidethis');

}


$(function() {
	$(".clickbutton").on("click", flip);
});




 $( function() {
    $( "#tabs" ).tabs();
  } );


 function goToNext() {
  $(this).parent().toggleClass('hidethis');
  $(this).parent().next().toggleClass('hidethis');
 }

 function goToPrev() {
  $(this).parent().toggleClass('hidethis');
  $(this).parent().prev().toggleClass('hidethis');
 }




$(function() {
  $(".nextButton").on("click", goToNext);
});

$(function() {
  $(".prevButton").on("click", goToPrev);
});







$(document).ready(function (){
 if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
    $("#iosDl").hide();
 }
 if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
  $("#androidDl").hide();
 }
});


$(document).ready(function(){
  $('.bxslider').bxSlider();
});


(function($) {
    var $window = $(window),
        $html = $('.bxSliderAdd');

    function resize() {
        if ($window.width() < 1000) {
            return $html.addClass('bxslider');
        }

        $html.removeClass('bxslider');
    }

    $window
        .resize(resize)
        .trigger('resize');
})(jQuery);


var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        console.log("ios");
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);

    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
if (navigator.userAgent.match(/Android/i)) {
  console.log("ANDROID");
  var d = document.getElementById("ios");
  d.className += " hidethis";
}


if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
  console.log("IOS");
    var d = document.getElementById("android");
  d.className += " hidethis";
}
