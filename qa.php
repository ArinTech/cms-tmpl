<?php
include 'parts/head.php';
?>

<!-- --------------------------- NAVBAR ------------------------- -->

<body id="page-top" ng-app="contactApp">
	<section class="container-fluid boxOne" id="second"> 
		<header>
				<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
			        <div class="container-fluid">
			            <div class="navbar-header">
			                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			                	<span class="sr-only">Toggle navigation</span> &#9776; <i class="fa fa-bars"></i>
			                </button>
			                <a class href="index.php" id="logobox"><img src="img/Logo Tmpl.png" alt="logo" class="imgSmall" id="logoPicture"></a>
			            </div>
				        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				            <ul class="nav navbar-nav navbar-right" style="margin-right:25px">
				                <li>
				                    <a class="page-scroll" href="cases.php">Cases</a>
				                </li>
				                 <li>
				                    <a class="page-scroll" href="news.php">Nyheter</a>
				                 </li>
				                 <li>
				                    <a class="page-scroll" href="qa.php" style="color:#07589b; font-weight:400;">Q&A</a>
				                </li>                 
				                </ul>
				        </div>
			        </div>
			   </nav>
			</header>
		</section>	
	<section class="container-fluid" id="tabs">

		<?php 
		include 'parts/qa_tabs.php';
		include 'parts/qa_articles.php';
		?>

	</section>

	<?php 
	include 'parts/footer.php';
	include 'parts/script.php';

